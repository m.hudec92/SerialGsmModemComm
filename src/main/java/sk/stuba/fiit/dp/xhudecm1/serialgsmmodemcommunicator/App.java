/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator;

import java.util.logging.Level;
import java.util.logging.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGS;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageDeliveryStatusInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandListEmptyException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialPortOperationException;

/**
 *
 * @author martinhudec
 */
public class App {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(App.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
          
            Modem modem = new Modem("/dev/ttyUSB2", "SM");
            modem.setMessageDeliveryStatusCallback((ReceivedMessageDeliveryStatusInterface receivedDeliveryStatus) -> {
                logger.debug(receivedDeliveryStatus.getDeliveryStatus());
                logger.debug(receivedDeliveryStatus.getReceipientNumber().getNumber());
                logger.debug(receivedDeliveryStatus.getMessageIdentifierNumber());
            });

            modem.setMessageReceivedCallback((ReceivedMessageInterface receivedMessage) -> {
                logger.debug(receivedMessage.getMessagePdu());
            });
            
            for (int i = 0; i < 1; i++) {
                ATCOutCMGS output = (ATCOutCMGS) modem.sendSmsPduMode(255, ("cigancina " + i).getBytes(), "+421908632636");
                logger.debug("RESULT no " + i + " " + output.getResult() + " sent message ref number " + output.getReferenceNumber());
            }
        } catch (SerialPortOperationException ex) {
            logger.error(ex);
        } catch (CommandErrorException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CommandNotSupportedException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CommandListEmptyException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
}
