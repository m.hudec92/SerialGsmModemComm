/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandListEmptyException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotImplementedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ModemNotRespondingException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialPortOperationException;

/**
 *
 * @author martinhudec
 */
public class CommandController {

    private List<Command> commandList = new ArrayList<>();
    final static Logger logger = Logger.getLogger(CommandController.class);

    public void executeCommands() throws CommandListEmptyException, ModemNotRespondingException, CommandErrorException, CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException, NoResponseReadFromSerialPortException, NoResponseReadFromSerialPortException {
        if (commandList.isEmpty()) {
            throw new CommandListEmptyException("There is empty command list");
        }
        for (Iterator<Command> iterator = commandList.iterator(); iterator.hasNext();) {
            
            Command command = iterator.next();
            logger.debug("executing command " + command.getClass().getName());
            iterator.remove();
            command.execute();
        }
    }

    public void addCommand(Command command) {
        commandList.add(command);
    }

}
