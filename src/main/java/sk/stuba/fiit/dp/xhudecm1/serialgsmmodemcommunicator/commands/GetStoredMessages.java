/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;

/**
 *
 * @author martinhudec
 */
public class GetStoredMessages implements Command {

    private final Modem modem;
    final static Logger logger = Logger.getLogger(GetStoredMessages.class);

    public GetStoredMessages(Modem modem) {
        this.modem = modem;
    }

    @Override
    public void execute() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException   {
        modem.getSelectedMessageStorageCapacity();
    }

}
