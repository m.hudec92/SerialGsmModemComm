/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.CommandController;

/**
 *
 * @author martinhudec
 */
public abstract class ATCInAbstract implements ATCInInterface {

    private String ATCommand;
    final static Logger logger = Logger.getLogger(ATCInAbstract.class);

    public ATCInAbstract(String ATCommand) {
        this.ATCommand = ATCommand;
    }

    @Override
    public String getATCommand() {
        return ATCommand;
    }

}
