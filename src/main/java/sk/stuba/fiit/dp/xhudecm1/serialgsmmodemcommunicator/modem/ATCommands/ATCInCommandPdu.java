/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import java.nio.ByteBuffer;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class ATCInCommandPdu extends ATCInAbstract {

    final static Logger logger = Logger.getLogger(ATCInCommandPdu.class);
    String destinationNumber = null;
    Integer messageRefNumber = null;
    Integer affectedMessageRefNum = null;

    public ATCInCommandPdu(Integer messageRefNumber, String destinationNumber, Integer affectedMessageRefNum) {
        super("AT+CMGS=");
        this.destinationNumber = destinationNumber;
        this.messageRefNumber = messageRefNumber;
        this.affectedMessageRefNum = affectedMessageRefNum;
    }

    @Override
    public String getATCommand() {
        return super.getATCommand(); //To change body of generated methods, choose Tools | Templates.
    }

    public byte[] getSmsPdu() {

        StringBuilder sb = new StringBuilder();
        sb.append("00").append("81")
                .append(Utils.intToHexFixedWidth(messageRefNumber, 2))
                .append("00") // TP-PID 
                .append("02") // TP-CT
                .append(Utils.intToHexFixedWidth(affectedMessageRefNum, 2))
                .append(Utils.intToHexFixedWidth(destinationNumber.length() - 1, 2))
                .append(Utils.AddrToPDU(destinationNumber))
                .append("00");
        logger.debug(sb.toString());
        byte[] pdu = Utils.toByteArray(sb.toString());
        long value = 0;
        for (int i = 0; i < pdu.length; i++) {
            value += ((long) pdu[i] & 0xffL) << (8 * i);
        }
        logger.debug("current value " + value);
        long srr = 1 << (8 * pdu.length);
        value = value | srr;
        long mti = 2 << (8 * pdu.length) + 9;
        value = value | mti;

        logger.debug("SMS DATA long representattion:  " + value);
        logger.debug(value);
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(value);
        logger.debug("SMS data hex " + Utils.toHexString(buffer.array()));
        return sb.toString().getBytes();
    }

    public Integer getSmsPduLength() {
        Integer length = getSmsPdu().length / 2;

        return length - 1;
    }

    protected static Integer setBitInHeader(Integer header, int startIndex, int endIndex, Integer value) {
        Integer length = (endIndex - startIndex) + 1;
        header <<= length;
        header |= value | (0 << length);
        logger.debug("CREATED BYTE " + header.byteValue() + " int value " + header + " set bit start " + startIndex + " end " + endIndex + " value " + value);
        return header;
    }
}
