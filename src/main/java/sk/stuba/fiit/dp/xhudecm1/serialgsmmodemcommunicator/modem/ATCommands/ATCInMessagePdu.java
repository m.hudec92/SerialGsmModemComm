/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import javax.xml.bind.DatatypeConverter;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class ATCInMessagePdu extends ATCInAbstract {

    final static Logger logger = Logger.getLogger(ATCInMessagePdu.class);
    byte[] payload = null;
    String destinationNumber = null;
    Integer messageRefNumber = null;

    public ATCInMessagePdu(Integer messageRefNumber, byte[] payload, String destinationNumber) {
        super("AT+CMGS=");
        this.payload = payload;
        this.destinationNumber = destinationNumber;
        this.messageRefNumber = messageRefNumber;
    }

    @Override
    public String getATCommand() {
        return super.getATCommand(); //To change body of generated methods, choose Tools | Templates.
    }

    public byte[] getSmsPdu() {
        StringBuilder sb = new StringBuilder();
        sb.append("00")
                .append("31")
                .append(Utils.intToHexFixedWidth(messageRefNumber, 2))
                .append(Utils.intToHexFixedWidth(destinationNumber.length() - 1, 2))
                .append("91")
                .append(Utils.AddrToPDU(destinationNumber))
                .append("00")
                .append("04")
                .append("FF")
                .append(Utils.intToHexFixedWidth(payload.length, 2))
                .append(Utils.toHexString(payload));
        logger.debug("SMS DATA HEX representation " + sb.toString());
        return sb.toString().getBytes();
    }

    public Integer getSmsPduLength() {
        Integer length = getSmsPdu().length / 2;

        return length - 1;
    }

}
