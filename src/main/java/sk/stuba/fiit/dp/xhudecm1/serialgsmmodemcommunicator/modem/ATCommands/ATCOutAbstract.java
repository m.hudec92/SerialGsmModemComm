/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.CommandController;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToParseSerialPortOutputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveSentMessageReferenceNumber;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnknownErrorException;

/**
 *
 * @author martinhudec
 */
public abstract class ATCOutAbstract implements ATCOutInterface {

    final static Logger logger = Logger.getLogger(ATCOutAbstract.class);
    protected ATCOutAbstract nextOutputType;
    protected String parseSequence;
    protected Modem modem;

    public void setNextATCommandOutput(ATCOutAbstract outputType) {
        this.nextOutputType = outputType;

    }

    public ATCOutAbstract(String parseSequence, Modem modem) {
        this.parseSequence = parseSequence;
        this.modem = modem;
    }

    @Override
    public ATCOutAbstract parseCommandOutput(String result, SerialComm serialComm) throws InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException, UnableToParseSerialPortOutputException, CommandErrorException, CommandNotSupportedException, UnknownErrorException, UnableToRetrieveNewMessageContentException, UnableToRetrieveSentMessageReferenceNumber {

        if (result.contains("OK") || result.contains("ERROR") || result.contains("COMMAND NOT SUPPORTED")) {

            ATCOutAbstract output = parse(result);
            if (nextOutputType != null) {
                if (result.contains("OK") && result.length() > "OK".length()) {
                    return nextOutputType.parseCommandOutput(result.replace("OK", "").trim(), serialComm, output);
                } else if (result.contains("ERROR") && result.length() > "ERROR".length()) {
                    return nextOutputType.parseCommandOutput(result.replace("ERROR", "").trim(), serialComm, output);
                } else if (result.contains("COMMAND NOT SUPPORTED") && result.length() > "COMMAND NOT SUPPORTED".length()) {
                    return nextOutputType.parseCommandOutput(result.replace("COMMAND NOT SUPPORTED", "").trim(), serialComm, output);
                }
            }
            return output;
        } else if (checkParserAvailability(result)) {
            return parse(result);
        } else if (nextOutputType != null) {
            return nextOutputType.parseCommandOutput(result, serialComm);
        } else {
            throw new UnableToParseSerialPortOutputException("No suitable parser found", result, serialComm);
        }
    }

    @Override
    public ATCOutAbstract parseCommandOutput(String result, SerialComm serialComm, ATCOutAbstract output) throws CommandErrorException, UnableToParseSerialPortOutputException, UnknownErrorException, UnableToRetrieveNewMessageContentException, UnknownErrorException, UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException, UnableToRetrieveSentMessageReferenceNumber {
        if (checkParserAvailability(result)) {

            return parse(result, output);
        } else if (nextOutputType != null) {
            return nextOutputType.parseCommandOutput(result, serialComm, output);
        } else {
            throw new UnableToParseSerialPortOutputException("No suitable parser found", result, serialComm);
        }
    }

    private boolean checkParserAvailability(String result) {
        logger.debug("TESTING " + result + " against " + parseSequence);
        if (!parseSequence.isEmpty()) {
            if (result.startsWith(parseSequence)) {
                logger.debug("Found parser for " + result + " " + this.getClass().getSimpleName());
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    abstract protected ATCOutAbstract parse(String result) throws UnableToRetrieveSentMessageReferenceNumber, UnknownErrorException, UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;

    abstract protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnableToRetrieveSentMessageReferenceNumber, UnknownErrorException, UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;

}
