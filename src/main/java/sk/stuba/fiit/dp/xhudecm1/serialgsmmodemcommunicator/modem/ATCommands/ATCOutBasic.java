/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotImplementedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveSentMessageReferenceNumber;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnknownErrorException;

/**
 *
 * @author martinhudec
 */
public class ATCOutBasic extends ATCOutAbstract {

    ATCommandResultEnum result = null;
    final static Logger logger = Logger.getLogger(ATCOutBasic.class);

    public ATCOutBasic(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }

    @Override
    protected ATCOutAbstract parse(String result) throws UnknownErrorException, UnableToRetrieveSentMessageReferenceNumber {
        if (result.contains("OK")) {
            this.result = ATCommandResultEnum.OK;
            return this;
        } else if (result.contains("ERROR")) {
            this.result = ATCommandResultEnum.ERROR;
            return this;
        } else if (result.contains("COMMAND NOT SUPPORT")) {
            this.result = ATCommandResultEnum.NOT_SUPPORTED;
            return this;
        } else {
            throw new UnknownErrorException("unknown result");
        }
    }

    public ATCommandResultEnum getResult() {
        return result;
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnableToRetrieveSentMessageReferenceNumber, UnsupportedOperationException, UnableToRetrieveNewMessageContentException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
