/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetPrefferedStorage;
import static sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGR.logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsAddress;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDUStatusReport;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;

/**
 *
 * @author martinhudec
 */
public class ATCOutCDS extends ATCOutAbstract implements ReceivedMessageDeliveryStatusInterface {

    final static Logger logger = Logger.getLogger(ATCOutCDS.class);
    private SmsPDUStatusReport pdu;

    public ATCOutCDS(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }

    @Override
    protected ATCOutAbstract parse(String result) throws UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        logger.debug("PARSE");

        String messageData;
        try {
            messageData = Utils.getRegexValue(result, "\\s.*$");
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            throw new UnableToRetrieveNewMessageContentException("Error retrieving data while reading SMS message", ex);
        }

        logger.debug("HEX DATA " + Utils.toHexString(Utils.toByteArray(messageData.trim())));

        pdu = new SmsPDUStatusReport(messageData.trim());
        logger.debug("NEW SMS PDU CREATED " + pdu);
        return this;
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnsupportedOperationException {
        logger.debug("RECEIVED +CDS TOGETHER WITH OK/ERROR/COMMAND NOT SUPPORTED");
        throw new UnsupportedOperationException("Unable to parse this kind of response yet"); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public SmsPDUStatusReport.DeliveryStatusType getDeliveryStatus() {
       return pdu.getDeliveryStatus();

    }

    @Override
    public Integer getMessageIdentifierNumber() {
        return pdu.getReferenceNumber();
    }

    @Override
    public SmsAddress getReceipientNumber() {
        return pdu.getSenderAddress();
    }

    @Override
    public SmsPDUStatusReport getStatusReport() {
        return pdu;
    }
}
