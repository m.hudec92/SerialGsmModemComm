/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import java.security.Timestamp;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;

/**
 *
 * @author martinhudec
 */
public class ATCOutCMGR extends ATCOutBasic {
    
    private SmsPDU smsPdu = null;
    final static Logger logger = Logger.getLogger(ATCOutCMGR.class);
    
    public ATCOutCMGR(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }
    
    @Override
    protected ATCOutAbstract parse(String result) throws UnsupportedOperationException {
        logger.debug("PARSE");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnableToRetrieveNewMessageContentException{
        
        String messageData;
        try {
            messageData = Utils.getRegexValue(result, "\\s.*$");
        } catch (Utils.UnableToFindValueWithRegexException ex) {
           throw  new UnableToRetrieveNewMessageContentException("Error retrieving data while reading SMS message", ex);
        }
        
        logger.debug("HEX DATA " + Utils.toHexString(Utils.toByteArray(messageData.trim())));
        super.result = ((ATCOutBasic) output).getResult();
        smsPdu = new SmsPDU(messageData.trim());
        logger.debug("NEW SMS PDU CREATED " + smsPdu);
        return this;
    }
    
    public SmsPDU getSmsPdu() {
        return smsPdu;
    }
    
}
