/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveSentMessageReferenceNumber;

/**
 *
 * @author martinhudec
 */
public class ATCOutCMGS extends ATCOutBasic {

    final static Logger logger = Logger.getLogger(ATCOutCMGS.class);

    private Integer referenceNumber = null;

    public ATCOutCMGS(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }

    @Override
    protected ATCOutAbstract parse(String result) throws UnsupportedOperationException {
        logger.debug("PARSE");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnableToRetrieveSentMessageReferenceNumber {
        logger.debug("PARSE WITH OUTPUT");
        logger.debug("RESULT TO BE PARSED \n" + result);
        super.result = ((ATCOutBasic) output).getResult();
        try {
            referenceNumber = Integer.parseInt(Utils.getRegexValue(result, "\\d.*$"));
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            throw new UnableToRetrieveSentMessageReferenceNumber("No reference number found", result, ex);
        }

        return this;
    }

    public Integer getReferenceNumber() {
        return referenceNumber;
    }

}
