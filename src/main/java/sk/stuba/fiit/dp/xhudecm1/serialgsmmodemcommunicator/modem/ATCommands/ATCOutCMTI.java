/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;

/**
 *
 * @author martinhudec
 */
public class ATCOutCMTI extends ATCOutAbstract implements ReceivedMessageInterface {

    final static Logger logger = Logger.getLogger(ATCOutCMTI.class);
    private SmsPDU pdu;

    public ATCOutCMTI(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }

    @Override
    protected ATCOutAbstract parse(String result) throws UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        logger.debug("PARSE");
        String storage;

        try {
            storage = Utils.getRegexValue(result, "\".*\"");
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            throw new UnableToRetrieveNewMessageContentException("No storage found in messageTrigger", result, ex);
        }

        logger.debug(storage);

        Integer recordNumber;

        try {

            recordNumber = Integer.parseInt(Utils.getRegexValue(result, "\\d.*$"));
        } catch (Utils.UnableToFindValueWithRegexException ex) {
            throw new UnableToRetrieveNewMessageContentException("No message record number found in messageTrigger", result);
        }

        logger.debug("Received new message stored in " + storage + " with record number " + recordNumber);
        ATCOutAbstract readMessage;
        readMessage = modem.readSmsMessage(recordNumber);
        
        logger.debug("Message read result " + ((ATCOutBasic) readMessage).getResult());
        pdu = ((ATCOutCMGR) readMessage).getSmsPdu();

        // teraz je potrebne este zmazat sms z pamete aby sme uvolnili miesto pre nove SMS 
        logger.debug("deleting message from modem ");
        ATCOutAbstract deleteMessage = modem.deleteSmsMessage(recordNumber);
        logger.debug("deleting message from modem result");

        return this;
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnsupportedOperationException {
        logger.debug("RECEIVED +CMTI TOGETHER WITH OK/ERROR/COMMAND NOT SUPPORTED");
        throw new UnsupportedOperationException("Unable to parse this kind of response yet"); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public SmsPDU getMessagePdu() {
        return pdu;
    }
}
