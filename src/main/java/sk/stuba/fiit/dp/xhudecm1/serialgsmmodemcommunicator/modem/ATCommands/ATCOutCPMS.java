/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import java.util.logging.Level;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetPrefferedStorage;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;

/**
 *
 * @author martinhudec
 */
public class ATCOutCPMS extends ATCOutAbstract {

    final static Logger logger = Logger.getLogger(ATCOutCPMS.class);

    public ATCOutCPMS(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }

    @Override
    protected ATCOutAbstract parse(String result) throws UnsupportedOperationException {
        logger.debug("PARSE");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) throws UnableToRetrieveNewMessageContentException {
        logger.debug("PARSE WITH OUTPUT");
        String[] cpmsData = result.split(",");
        if (cpmsData.length == 9) {
            logger.info("[" + modem.getModemSerialPort() + "][STAGE 8] GETTING MESSAGES WITH CPMS");
            logger.debug("received CPMS? result");
            logger.debug("received messages count " + cpmsData[1]);
            for (int i = 0; i < Integer.parseInt(cpmsData[1]); i++) {
                logger.debug("adding stored message id " + i + " to received message processing queue on modem " + modem.getModemSerialPort());
                logger.info("[" + modem.getModemSerialPort() + "][STAGE 10] ADDING TO RECEVIED MESSAGE QUEUE");
                modem.getSerialComm().getReceivedMessageQueue().add("+CMTI: \"" + Modem.STORAGE + "\"," + i);

            }
        }
        return output;
    }

}
