/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetPrefferedStorage;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;

/**
 *
 * @author martinhudec
 */
public class ATCOutCSMS extends ATCOutAbstract {

    final static Logger logger = Logger.getLogger(ATCOutCSMS.class);

    public ATCOutCSMS(String parseSequence, Modem modem) {
        super(parseSequence, modem);
    }



    @Override
    protected ATCOutAbstract parse(String result)  {
        logger.debug("PARSE");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ATCOutAbstract parse(String result, ATCOutAbstract output) {
        logger.debug("PARSE WITH OUTPUT");
        return output;
    }

}
