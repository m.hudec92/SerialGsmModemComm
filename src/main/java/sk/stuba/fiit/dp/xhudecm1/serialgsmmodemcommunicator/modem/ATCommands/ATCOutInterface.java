/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToParseSerialPortOutputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveSentMessageReferenceNumber;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnknownErrorException;

/**
 *
 * @author martinhudec
 */
public interface ATCOutInterface {

    public ATCOutAbstract parseCommandOutput(String result, SerialComm serialComm) throws UnableToRetrieveSentMessageReferenceNumber, CommandErrorException, UnableToRetrieveNewMessageContentException, UnableToParseSerialPortOutputException, UnknownErrorException, UnableToRetrieveNewMessageContentException, UnknownErrorException, UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;

    public ATCOutAbstract parseCommandOutput(String result, SerialComm serialComm, ATCOutAbstract output) throws UnableToRetrieveSentMessageReferenceNumber, CommandErrorException, UnableToRetrieveNewMessageContentException, UnableToParseSerialPortOutputException, UnknownErrorException, UnableToRetrieveNewMessageContentException, UnknownErrorException, UnableToRetrieveNewMessageContentException, CommandErrorException, CommandNotSupportedException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;
}
