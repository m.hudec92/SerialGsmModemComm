/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsAddress;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDUStatusReport;

/**
 *
 * @author martinhudec
 */
public interface ReceivedMessageDeliveryStatusInterface {

    public SmsPDUStatusReport.DeliveryStatusType getDeliveryStatus();

    public Integer getMessageIdentifierNumber();

    public SmsAddress getReceipientNumber();

    public SmsPDUStatusReport getStatusReport();
}
