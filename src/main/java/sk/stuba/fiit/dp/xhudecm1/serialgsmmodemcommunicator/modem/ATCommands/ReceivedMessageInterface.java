/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;

/**
 *
 * @author martinhudec
 */
public interface ReceivedMessageInterface {

    public SmsPDU getMessagePdu();
}
