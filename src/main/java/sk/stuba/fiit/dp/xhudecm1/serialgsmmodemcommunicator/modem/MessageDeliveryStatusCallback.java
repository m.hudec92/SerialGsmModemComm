/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageDeliveryStatusInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageInterface;

/**
 *
 * @author martinhudec
 */
public interface MessageDeliveryStatusCallback {
    public void processDeliveryStatus(ReceivedMessageDeliveryStatusInterface receivedDeliveryStatus);
}
