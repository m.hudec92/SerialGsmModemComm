/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem;

import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.CommandController;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.GetServiceCenterAddress;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.GetStoredMessages;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetCnmi;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetMessageService;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetPduMode;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.SetPrefferedStorage;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.TestSerialCommunication;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInMessagePdu;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutBasic;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMTI;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCommandResultEnum;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.BasicATCommand;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialCommController;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandListEmptyException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ModemNotRespondingException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToOpenSerialPortException;

/**
 *
 * @author martinhudec
 */
public class Modem implements ModemInterface {

    private SerialComm serialComm = null;
    private String modemSerialPort = null;
    final static Logger logger = Logger.getLogger(Modem.class);
    public static String STORAGE = null;
    private CommandController commandController = null;
    private MessageReceivedCallback receivedMessageCallback = null;
    private MessageDeliveryStatusCallback receivedDeliveryStatusCallback = null;

    public Modem(String modemSerialPort, String smsMessageStorage) throws CommandListEmptyException, CommandErrorException, ErrorWritingATCommandToSerialPortException, ErrorWritingATCommandToSerialPortException, ErrorWritingATCommandToSerialPortException, ModemNotRespondingException, ModemNotRespondingException, UnableToOpenSerialPortException, UnableToOpenSerialPortException, CommandNotSupportedException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.debug("CREATING NEW MODEM for serial port " + modemSerialPort);
        this.modemSerialPort = modemSerialPort;
        STORAGE = smsMessageStorage;
        this.commandController = new CommandController();
        initSerialPortComm();
    }

    private void initSerialPortComm() throws CommandListEmptyException, ErrorWritingATCommandToSerialPortException, ModemNotRespondingException, UnableToOpenSerialPortException, ModemNotRespondingException, ModemNotRespondingException, CommandErrorException, CommandErrorException, CommandNotSupportedException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, ErrorWritingATCommandToSerialPortException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException, ErrorWritingATCommandToSerialPortException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        serialComm = SerialCommController.getInstance(this);
        serialComm.initSerialComm();
        //setNoEchoMode();
        commandController.addCommand(new TestSerialCommunication(this));
        commandController.executeCommands();
        // resetModem();
        // setTriggerMessagesOn();
        //pollAllStoredMessages();
        //deleteAllMessages();
    }

    public String getModemSerialPort() {
        return modemSerialPort;
    }

    @Override
    public void testModemInterface() throws ModemNotRespondingException {
        ATCOutBasic output;
        try {
            logger.info("[" + modemSerialPort + "][STAGE 1] TESTING MODEM IF");
            output = (ATCOutBasic) serialComm.writeATCommand(new BasicATCommand("AT"));
        } catch (Exception ex) {
            throw new ModemNotRespondingException("Modem not initialized succesfully", serialComm);
        }

        if (output.getResult().equals(ATCommandResultEnum.OK)) {
            try {
                logger.info("Modem on serial port: " + modemSerialPort + " was succesfully initialized");
                
                commandController.addCommand(new SetCnmi(this));
                commandController.addCommand(new SetPduMode(this));
                commandController.addCommand(new SetPrefferedStorage(this));
                commandController.addCommand(new SetMessageService(this));
                commandController.addCommand(new GetServiceCenterAddress(this));
                commandController.executeCommands();
                
                commandController.addCommand(new GetStoredMessages(this));
                commandController.executeCommands();
            } catch (CommandListEmptyException | CommandErrorException | CommandNotSupportedException | ErrorWritingATCommandToSerialPortException | InvalidAtCommandInputException | NoResponseReadFromSerialPortException ex) {
                throw new ModemNotRespondingException("Modem not initialized succesfully", serialComm);
            }
        } else {
            throw new ModemNotRespondingException("Modem not initialized succesfully", serialComm);
        }
    }

    private void resetModem() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("ATZ"));
    }

    private void setNoEchoMode() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("ATE0"));
    }

    private void setTriggerMessagesOn() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("ATQ0"));
    }

    @Override
    public void setCnmi() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.info("[" + modemSerialPort + "][STAGE 2] SETTING CNMI");
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CNMI=1,1,0,1"));
        
    }

    @Override
    public void setPduMode() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.info("[" + modemSerialPort + "][STAGE 3] SETTING PDU MODE ");

        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CMGF=0"));
    }

    @Override
    public void setMessageService() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.info("[" + modemSerialPort + "][STAGE 4] SETTING CSMS");

        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CSMS=0"));
    }

    @Override
    public void setPrefferedMessageStorage() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {

        logger.info("[" + modemSerialPort + "][STAGE 5] SETTING PREF MESSAGE STORAGE");
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CPMS=\"" + STORAGE + "\",\"" + STORAGE + "\",\"" + STORAGE + "\""));
    }

    @Override
    public void getAvailableMessageStorage() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.info("[" + modemSerialPort + "][STAGE 6] GETTING AVAILABLE MESSAGE STORAGE");

        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CPMS=?"));
    }

    @Override
    public void getSelectedMessageStorageCapacity() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
    if ("/dev/ttyUSB5".equals(this.modemSerialPort)){
        System.out.println("daco");
    }
        logger.info("[" + modemSerialPort + "][STAGE 7] GETTING MESSAGES");

    ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CPMS?"));
    }

    @Override
    public void getServiceCenterAddress() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CSCA?"));
    }

    @Override
    public List<ATCOutCMTI> pollAllStoredMessages() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {

        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CMGL"));
        ATCOutAbstract outputReadMessages = serialComm.writeATCommand(new BasicATCommand("AT+CMGL=4"));

        return null;
    }

    private void deleteAllMessages() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        for (int i = 0; i < 30; i++) {

            ATCOutAbstract outputReadMessages = serialComm.writeATCommand(new BasicATCommand("AT+CMGD=" + i));
        }

    }

    @Override
    public ATCOutAbstract sendSmsPduMode(Integer refNumber, byte[] payload, String destinationNumber) throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException {
        logger.debug("Sending SMS pdu mode");
        return serialComm.writeMessagePDU(new ATCInMessagePdu(refNumber, payload, destinationNumber));
    }

    @Override
    public MessageReceivedCallback getMessageReceivedCallback() {
        return receivedMessageCallback;
    }

    @Override
    public MessageDeliveryStatusCallback getReceivedDeliveryStatusCallback() {
        return receivedDeliveryStatusCallback;
    }

    @Override
    public void setMessageReceivedCallback(MessageReceivedCallback messageReceivedCallback) {
        this.receivedMessageCallback = messageReceivedCallback;
    }

    @Override
    public void setMessageDeliveryStatusCallback(MessageDeliveryStatusCallback messageDeliveryStatusCallback) {
        this.receivedDeliveryStatusCallback = messageDeliveryStatusCallback;
    }

    @Override
    public CommandController getCommandController() {
        return commandController;
    }

    @Override
    public void setSmsStorage(String storageName) {
        STORAGE = storageName;
    }

    @Override
    public ATCOutAbstract readSmsMessage(Integer recordNumber) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output = serialComm.writeATCommand(new BasicATCommand("AT+CMGR=" + recordNumber));
        return output;
    }

    @Override
    public ATCOutAbstract deleteSmsMessage(Integer recordNumber) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        ATCOutAbstract output;
        output = serialComm.writeATCommand(new BasicATCommand("AT+CMGD=" + recordNumber));

        return output;
    }

    public SerialComm getSerialComm() {
        return serialComm;
    }

}
