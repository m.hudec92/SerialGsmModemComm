/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem;

import java.util.List;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.commands.CommandController;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMTI;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ModemNotRespondingException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.SerialPortOperationException;

/**
 *
 * @author martinhudec
 */
public interface ModemInterface {

    public void testModemInterface() throws ModemNotRespondingException;

    public void setCnmi() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void setPduMode() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void setMessageService() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void setPrefferedMessageStorage() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void getAvailableMessageStorage() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void getSelectedMessageStorageCapacity() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void getServiceCenterAddress() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public ATCOutAbstract sendSmsPduMode(Integer refNumber, byte[] payload, String destinationNumber) throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void setMessageReceivedCallback(MessageReceivedCallback messageReceivedCallback);

    public void setMessageDeliveryStatusCallback(MessageDeliveryStatusCallback messageDeliveryStatusCallback);

    public List<ATCOutCMTI> pollAllStoredMessages() throws CommandErrorException, CommandNotSupportedException, ErrorWritingATCommandToSerialPortException, InvalidAtCommandInputException, NoResponseReadFromSerialPortException;

    public void setSmsStorage(String storageName);

    public ATCOutAbstract readSmsMessage(Integer recordNumber) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;

    public ATCOutAbstract deleteSmsMessage(Integer recordNumber) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException;

    public MessageReceivedCallback getMessageReceivedCallback();

    public MessageDeliveryStatusCallback getReceivedDeliveryStatusCallback();

    public CommandController getCommandController();
}
