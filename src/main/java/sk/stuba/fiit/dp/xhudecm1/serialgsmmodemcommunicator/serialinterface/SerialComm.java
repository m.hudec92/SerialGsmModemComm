/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInAbstract;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInMessagePdu;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCPMS;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutBasic;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCDS;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGL;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGR;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGS;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMTI;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageDeliveryStatusInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ReceivedMessageInterface;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandErrorException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.CommandNotSupportedException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.ErrorWritingATCommandToSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.InvalidAtCommandInputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.NoResponseReadFromSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToOpenSerialPortException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToParseSerialPortOutputException;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnknownErrorException;

/**
 *
 * @author martinhudec
 */
public class SerialComm implements SerialCommInterface {

    final static Logger logger = Logger.getLogger(SerialComm.class);
    private static SerialComm serialComm = null;
    private String serialComPortName = null;
    private SerialPort port = null;
    private static Modem MODEM = null;
    private Queue<ATCOutAbstract> serialReadQueue = null;
    private Queue<String> receivedMessageQueue = null;
    private Queue<ATCOutAbstract> messageSuccesfullySentQueue = null;
    private Queue<String> messageDeliveredSucessfullyQueue = null;
    private Queue<ATCInAbstract> messagesToWrite = null;
    private Boolean messageSendingAvailableLock = null;
    private Boolean serialPortAvailableLock = true;
    private final byte[] CTRLZ = new byte[]{(byte) 0x1A};
    private Integer messageCounter = 0;

    public SerialComm(Modem modem) {
        MODEM = modem;
        serialComPortName = modem.getModemSerialPort();
        init();
    }

    private void init() {
        serialComm = this;
    }

    @Override
    public void closeSerialComm() {
        port.closePort();
        logger.debug("New serialCommSingleton created for port name: " + serialComPortName);
    }

    @Override

    public void initSerialComm() throws UnableToOpenSerialPortException {

        port = SerialPort.getCommPort(serialComPortName);
        port.setBaudRate(9600);
        //port.setFlowControl(SerialPort.FLOW_CONTROL_RTS_ENABLED | SerialPort.FLOW_CONTROL_CTS_ENABLED | SerialPort.FLOW_CONTROL_XONXOFF_IN_ENABLED | SerialPort.FLOW_CONTROL_XONXOFF_OUT_ENABLED);
        port.setNumDataBits(8);

        serialReadQueue = new LinkedList<>();
        receivedMessageQueue = new LinkedList<>();
        messageSuccesfullySentQueue = new LinkedList<>();
        messageDeliveredSucessfullyQueue = new LinkedList<>();
        messagesToWrite = new LinkedList<>();
        new Thread(new MessageResponseHandlerThread(MODEM)).start();
        if (!port.openPort()) {
            throw new UnableToOpenSerialPortException("Unable to open serial port", this);
        }
        port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 500, 0);
        //port.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
        if (port.bytesAvailable() > 0) {
            logger.warn("Clearing serial port");
            try {
                while (port.bytesAvailable() > 0) {
                    byte[] readBuffer = new byte[1024];
                    int numRead = port.readBytes(readBuffer, readBuffer.length);
                    logger.debug("Dropped data from serial port " + numRead + " bytes.");
                }
            } catch (Exception e) {
                logger.error("Unable to drop data from serial port", e);
            }
        }
        port.addDataListener(new SerialPortDataListener() {

            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(SerialPortEvent spe) {
                if (spe.getSerialPort().bytesAvailable() > 2) {

                    byte[] readBuffer = new byte[spe.getSerialPort().bytesAvailable()];
                    int numRead = spe.getSerialPort().readBytes(readBuffer, readBuffer.length);
                    String newData = new String(readBuffer).trim();
                    logger.debug("NEW DATA " + newData + "\n" + Utils.toHexString(newData.getBytes()));
                    String newDataStripped = newData.replaceFirst("AT.*(.*\\s)", "").trim();
                    if (!newData.equals(newDataStripped)) {
                        logger.debug("STRIPPED " + newDataStripped);
                    }

                    if (newData.startsWith("+CMTI")) {

                        logger.debug("Received new message trigger");
                        receivedMessageQueue.add((newData).trim());
                    } else if (newData.startsWith("+CMGS")) {
                        try {
                            messageSuccesfullySentQueue.add(getOutputParsers().parseCommandOutput(newData, serialComm));
                        } catch (Exception ex) {
                            logger.error("Error while parsing data received on serial port", ex);
                        }
                    } else if (newData.startsWith("+CDS")) {
                        messageDeliveredSucessfullyQueue.add((newData).trim());
                    } else if (newData.contains(">")) {
                        messageSendingAvailableLock = false;
                    } else if (newData.startsWith("+")
                            || newData.startsWith("OK")
                            || newData.startsWith("ERROR")
                            || newData.startsWith("COMMAND NOT SUPPORTED")) {
                        logger.debug("Serial event triggered");
                        try {

                            serialReadQueue.add(getOutputParsers().parseCommandOutput(newData, serialComm));
                        } catch (UnableToParseSerialPortOutputException | UnknownErrorException ex) {
                            logger.error("Error while parsing data received on serial port", ex);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(SerialComm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if (newDataStripped.startsWith("+")
                            || newDataStripped.startsWith("OK")
                            || newDataStripped.startsWith("ERROR")
                            || newDataStripped.startsWith("COMMAND NOT SUPPORTED")) {
                        try {

                            serialReadQueue.add(getOutputParsers().parseCommandOutput(newDataStripped, serialComm));
                        } catch (UnableToParseSerialPortOutputException | UnknownErrorException ex) {
                            logger.error("Error while parsing data received on serial port", ex);
                        } catch (Exception ex) {
                            java.util.logging.Logger.getLogger(SerialComm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        logger.debug("Serial data received was dropped :" + new String(readBuffer).trim());
                        logger.debug("Serial data received was dropped byte representation :" + Utils.toHexString(readBuffer));
                    }
                }
            }
        });
    }

    @Override
    synchronized public ATCOutAbstract writeMessagePDU(ATCInAbstract atCommand) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        messageSuccesfullySentQueue.clear();
        if (!(atCommand instanceof ATCInMessagePdu)) {
            throw new InvalidAtCommandInputException("Invalid AT command input while sending asynch SMS", atCommand, serialComm);
        }
        serialPortAvailableLock = false;
        String atCommandToBeWritten = atCommand.getATCommand() + "\r";
        byte[] smsData = null;

        logger.debug("Writing AT command for sending SMS message " + atCommandToBeWritten + " on serial port " + serialComPortName);
        smsData = ((ATCInMessagePdu) atCommand).getSmsPdu();
        logger.debug("sending sms with WRITE MESSAGE PDU " + Utils.toHexString(smsData));
        atCommandToBeWritten = atCommand.getATCommand() + ((ATCInMessagePdu) atCommand).getSmsPduLength() + "\r";
        messageSendingAvailableLock = true;

        Integer bytesWritten = port.writeBytes(atCommandToBeWritten.getBytes(), atCommandToBeWritten.getBytes().length);
        if (bytesWritten < 0) {
            throw new ErrorWritingATCommandToSerialPortException("Operation doesn't return possitive bytes written", atCommand, this);
        }

        logger.debug("New At Command sent: " + atCommandToBeWritten + " bytes written " + bytesWritten);

        logger.debug("waiting for payload sending available");
        while (messageSendingAvailableLock) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                throw new NoResponseReadFromSerialPortException("Exception during waiting for result", atCommand, serialComm, ex);
            }
        }
        logger.info("Writing SMS pdu to serial port");
        bytesWritten = port.writeBytes(smsData, smsData.length);

        logger.debug("Writing SMS data: " + Utils.toHexString(smsData) + " bytes written " + bytesWritten);
        if (bytesWritten < 0) {
            throw new ErrorWritingATCommandToSerialPortException("Operation doesn't return possitive bytes written", atCommand, this);
        }

        bytesWritten = port.writeBytes(CTRLZ, CTRLZ.length);
        logger.debug("Writing ctr+z : " + Utils.toHexString(CTRLZ) + " bytes written " + bytesWritten);
        if (bytesWritten < 0) {
            throw new ErrorWritingATCommandToSerialPortException("Operation doesn't return possitive bytes written", atCommand, this);
        }

        Integer count = 0;
        while (messageSuccesfullySentQueue.isEmpty()) {
            if (count > 1000) {

                logger.debug("throwing exception");
                throw new NoResponseReadFromSerialPortException("Exception during waiting for result", atCommand, serialComm);

            }
            // logger.debug("Waiting for result");
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                throw new NoResponseReadFromSerialPortException("Exception during waiting for result", atCommand, serialComm, ex);

            }
            count++;
        }

        ATCOutAbstract result = messageSuccesfullySentQueue.poll();
        switch (((ATCOutBasic) result).getResult()) {
            case OK: {
                logger.debug("Command " + atCommand.getATCommand().trim() + " executed OK");
                break;
            }
            case ERROR: {
                logger.error("Command " + atCommand.getATCommand().trim() + " executed with error Result");
                throw new CommandErrorException("Command " + atCommand.getATCommand().trim() + " executed with error Result");
            }
            case NOT_SUPPORTED: {
                throw new CommandNotSupportedException("Command " + atCommand.getATCommand().trim() + " is not supported by current modem");
            }
        }
        return result;
    }

    @Override
    synchronized public ATCOutAbstract writeATCommand(ATCInAbstract atCommand) throws CommandNotSupportedException, CommandErrorException, InvalidAtCommandInputException, ErrorWritingATCommandToSerialPortException, NoResponseReadFromSerialPortException {
        logger.debug("writing new AT COMMAND " + atCommand.toString() + " on serial port " + serialComPortName);
        String atCommandToBeWritten = atCommand.getATCommand() + "\r";

        Integer bytesWritten = port.writeBytes(atCommandToBeWritten.getBytes(), atCommandToBeWritten.getBytes().length);
        if (bytesWritten < 0) {
            throw new ErrorWritingATCommandToSerialPortException("Operation doesn't return possitive bytes written", atCommand, this);
        }
        try {
            Thread.sleep(5);
        } catch (InterruptedException ex) {
            throw new NoResponseReadFromSerialPortException("Exception during waiting for result", atCommand, this, ex);
        }

        Integer count = 0;
        while (serialReadQueue.isEmpty()) {
            if (count > 1000) {
                throw new NoResponseReadFromSerialPortException("No response returned", atCommand, this);
            }
            // logger.debug("Waiting for result");
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                throw new NoResponseReadFromSerialPortException("Exception during waiting for result", atCommand, this, ex);
            }
            count++;
        }

        ATCOutAbstract result = serialReadQueue.poll();
        switch (((ATCOutBasic) result).getResult()) {
            case OK: {
                logger.debug("Command " + atCommand.getATCommand().trim() + " executed OK");
                break;
            }
            case ERROR: {
                logger.error("Command " + atCommand.getATCommand().trim() + " executed with error Result");
                throw new CommandErrorException("Command " + atCommand.getATCommand().trim() + " executed with error Result");
            }
            case NOT_SUPPORTED: {
                throw new CommandNotSupportedException("Command " + atCommand.getATCommand().trim() + " is not supported by current modem");
            }
        }
        return result;
    }

    @Override
    public String getSerialComPortName() {
        return serialComPortName;
    }

    private static ATCOutAbstract getOutputParsers() {

        ATCOutAbstract result = new ATCOutBasic("", MODEM);
        ATCOutAbstract resultCPMS = new ATCOutCPMS("+CPMS", MODEM);
        ATCOutAbstract resultCSMS = new ATCOutCPMS("+CSMS", MODEM);
        ATCOutAbstract resultSCSA = new ATCOutCPMS("+CSCA", MODEM);
        ATCOutAbstract resultCMTI = new ATCOutCMTI("+CMTI", MODEM);
        ATCOutAbstract resultCMGS = new ATCOutCMGS("+CMGS", MODEM);
        ATCOutAbstract resultCMGL = new ATCOutCMGL("+CMGL", MODEM);
        ATCOutAbstract resultCMGR = new ATCOutCMGR("+CMGR", MODEM);
        ATCOutAbstract resultCDS = new ATCOutCDS("+CDS", MODEM);

        result.setNextATCommandOutput(resultCPMS);
        resultCPMS.setNextATCommandOutput(resultCSMS);
        resultCSMS.setNextATCommandOutput(resultSCSA);
        resultSCSA.setNextATCommandOutput(resultCMTI);
        resultCMTI.setNextATCommandOutput(resultCMGS);
        resultCMGS.setNextATCommandOutput(resultCMGL);
        resultCMGL.setNextATCommandOutput(resultCMGR);
        resultCMGR.setNextATCommandOutput(resultCDS);
        return result;
    }

    public SerialPort getPort() {
        return port;
    }

    private class MessageResponseHandlerThread implements Runnable {

        Modem modem;

        public MessageResponseHandlerThread(Modem modem) {
            this.modem = modem;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (!receivedMessageQueue.isEmpty()) {
                        try {
                            logger.info("[" + modem.getModemSerialPort() + "][STAGE 9] POLLING MESSAGE FROM RECEIVED MESSAGE QUEUE");    
                            ATCOutAbstract parsedMessage = getOutputParsers().parseCommandOutput(receivedMessageQueue.poll(), serialComm); 
                            if (modem.getMessageReceivedCallback() != null) {
                                modem.getMessageReceivedCallback().processReceivedMessage((ReceivedMessageInterface) parsedMessage);
                            }
                        } catch (Exception ex) {
                            logger.error("Unable to call callback after message received", ex);
                        }
                    }
                    if (!messageDeliveredSucessfullyQueue.isEmpty()) {
                        try {
                            ATCOutCDS parsedMessage = (ATCOutCDS) getOutputParsers().parseCommandOutput(messageDeliveredSucessfullyQueue.poll(), serialComm);
                            if (modem.getReceivedDeliveryStatusCallback() != null) {
                                modem.getReceivedDeliveryStatusCallback().processDeliveryStatus((ReceivedMessageDeliveryStatusInterface) parsedMessage);
                            }
                        } catch (Exception ex) {
                            logger.error("Unable to call callback after message received", ex);
                        }
                    }

                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    logger.error("Error in async message writing thread", ex);
                }
            }
        }
    }

    public Queue<String> getReceivedMessageQueue() {
        return receivedMessageQueue;
    }

}
