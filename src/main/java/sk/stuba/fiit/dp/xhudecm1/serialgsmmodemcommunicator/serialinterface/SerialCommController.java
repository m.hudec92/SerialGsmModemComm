/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.Modem;

/**
 *
 * @author martinhudec
 */
public class SerialCommController {

    private static final Map<Modem, SerialComm> serialCommSingletonMap = new HashMap<>();
    final static Logger logger = Logger.getLogger(SerialCommController.class);

    public static SerialComm getInstance(Modem modem) {
        if (!serialCommSingletonMap.containsKey(modem)) {
            serialCommSingletonMap.put(modem, new SerialComm(modem));
            logger.debug("Adding new modem to serialComControlleru " + modem.getModemSerialPort());
        } else {
            logger.debug("Using already stored modem in serialComControlleru " + modem.getModemSerialPort());
        }
        return serialCommSingletonMap.get(modem);
    }
}
