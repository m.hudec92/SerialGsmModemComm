/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class SmsAddress {

    private NumberingPlan numberingPlan = null;
    private NumberType numberType = null;
    private Integer addressType = null;
    private String number = null;
    private Integer numberLength = null;

    SmsAddress(String hexStringAddressType, Integer numberLength, String hexStringNumber) {
        this.numberLength = numberLength;
        try {
            addressType = Integer.parseInt(hexStringAddressType, 16);
        } catch (NumberFormatException numberFormatException) {
            addressType = 0;
        }
        numberType = NumberType.getEnumValue(Utils.unsignedIntFromInteger(addressType, 4, 6));
        numberingPlan = NumberingPlan.getEnumValue(Utils.unsignedIntFromInteger(addressType, 0, 3));
        parseNumber(hexStringNumber);
    }

    private String parseNumber(String hexStringNumber) {
        if (number == null) {
            this.number = Utils.parseNumberFromHexStringPDU(hexStringNumber, this, numberLength);
        }
        return number;
    }

    public String toString() {
        String smsAddress = "SMS address number: " + number + " numberType " + numberType + " numbering plan " + numberingPlan;
        return smsAddress;
    }

    public enum NumberType {

        UNKNOWN(0), INTERNATIONAL(1), NATIONAL(2), NETWORK(3), SUBSCRIBER(4), ALPHANUMERIC(5), ABBREVIATED(6), RESERVED(7);
        private final int value;

        NumberType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static NumberType getEnumValue(Integer value) {
            return NumberType.values()[value];

        }
    }

    public enum NumberingPlan {

        UNKNOWN(0), TELEPHONE(1), DATA(3), TELEX(4), NATIONAL(8), PRIVATE(9), ERMES(10), RESERVED(15);

        private final int value;

        NumberingPlan(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static NumberingPlan getEnumValue(Integer value) {
            return NumberingPlan.values()[value];

        }
    }

    public NumberType getNumberType() {
        return numberType;
    }

    public NumberingPlan getNumberingPlan() {
        return numberingPlan;
    }

    public String getNumber() {
        return number;
    }

}
