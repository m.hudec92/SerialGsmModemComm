/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class SmsPDU {

    private String smsPduHexString = null;
    private SmsAddress smscAddress = null;
    private SmsAddress senderAddress = null;
    private SmsTimeStamp smsTimeStamp = null;
    private String smsDataPayloadHexString = null;

    final static Logger logger = Logger.getLogger(SmsPDU.class);

    public SmsPDU(String smsPduHexString) {
        this.smsPduHexString = smsPduHexString;
        parseMessagePDU(smsPduHexString);
    }

    private void parseMessagePDU(String hexString) {
        Integer smscNumberLength = null;
        Integer senderNumberLength = null;
        Integer userDataLength = null;

        smsPduHexString = hexString;
        int index = 0;
        smscNumberLength = Integer.parseInt(smsPduHexString.substring(index, index + 2), 16);

        index += 2;
        if (smscNumberLength > 0) {
            String smscAddressType = smsPduHexString.substring(index, index + 2);
            index += 2;
            String smscNumber = smsPduHexString.substring(index, (smscNumberLength + 1) * 2);
            smscAddress = new SmsAddress(smscAddressType, smscNumberLength, smscNumber);
            index = (smscNumberLength + 1) * 2;
        }
        index += 2;

        senderNumberLength = Integer.parseInt(smsPduHexString.substring(index, index + 2), 16);

        index += 2;
        String senderAddressType = smsPduHexString.substring(index, index + 2);

        index += 2;
        String senderNumber = smsPduHexString.substring(index, index + (senderNumberLength % 2 == 0 ? senderNumberLength : senderNumberLength + 1));

        index += (senderNumberLength % 2 == 0 ? senderNumberLength : senderNumberLength + 1);
        senderAddress = new SmsAddress(senderAddressType, senderNumberLength, senderNumber);
        index += 2;
        index += 2;

        smsTimeStamp = new SmsTimeStamp(smsPduHexString.substring(index, index + (7 * 2)));

        index += 7 * 2;

        userDataLength = Integer.parseInt(smsPduHexString.substring(index, index + 2), 16);

        index += 2;
        smsDataPayloadHexString = smsPduHexString.substring(index);
    }

    @Override
    public String toString() {
        return "From :" + senderAddress.getNumber() + " data: " + smsDataPayloadHexString + " time: " + smsTimeStamp;
    }

    public String getSmsPduHexString() {
        return smsPduHexString;
    }

    public SmsAddress getSmscAddress() {
        return smscAddress;
    }

    public SmsAddress getSenderAddress() {
        return senderAddress;
    }

    public SmsTimeStamp getSmsTimeStamp() {
        return smsTimeStamp;
    }

    public String getSmsDataPayloadHexString() {
        return smsDataPayloadHexString;
    }

}
