/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms;

import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class SmsPDUStatusReport {

    private String smsPduHexString = null;
    private SmsAddress smscAddress = null;
    private SmsAddress senderAddress = null;
    private SmsTimeStamp smsTimeStamp = null;
    private Integer referenceNumber = null;
    private DeliveryStatusType deliveryStatus = null;

    final static Logger logger = Logger.getLogger(SmsPDUStatusReport.class);

    public SmsPDUStatusReport(String smsPduHexString) {
        this.smsPduHexString = smsPduHexString;
        parseMessagePDU(smsPduHexString);
    }

    private void parseMessagePDU(String hexString) {
        Integer smscNumberLength = null;
        Integer senderNumberLength = null;
        Integer deliveryStatusInteger = null;

        smsPduHexString = hexString;
        int idx = 0;
        smscNumberLength = Integer.parseInt(smsPduHexString.substring(idx, idx + 2), 16);

        idx += 2;
        if (smscNumberLength > 0) {
            String smscAddressType = smsPduHexString.substring(idx, idx + 2);
            idx += 2;
            String smscNumber = smsPduHexString.substring(idx, (smscNumberLength + 1) * 2);
            smscAddress = new SmsAddress(smscAddressType, smscNumberLength, smscNumber);
            idx = (smscNumberLength + 1) * 2;
        }
        idx += 2;

        referenceNumber = Integer.parseInt(smsPduHexString.substring(idx, idx + 2), 16);

        idx += 2;

        senderNumberLength = Integer.parseInt(smsPduHexString.substring(idx, idx + 2), 16);

        idx += 2;
        String senderAddressType = smsPduHexString.substring(idx, idx + 2);

        idx += 2;
        String senderNumber = smsPduHexString.substring(idx, idx + (senderNumberLength % 2 == 0 ? senderNumberLength : senderNumberLength + 1));

        idx += (senderNumberLength % 2 == 0 ? senderNumberLength : senderNumberLength + 1);
        senderAddress = new SmsAddress(senderAddressType, senderNumberLength, senderNumber);
        idx += 2;
        idx += 2;

        smsTimeStamp = new SmsTimeStamp(smsPduHexString.substring(idx, idx + (7 * 2)));

        idx += 7 * 2;

        deliveryStatusInteger = Integer.parseInt(smsPduHexString.substring(idx, idx + 1), 16);
        logger.debug("DELIVERY STATUS INTEGER VALUE " + deliveryStatusInteger);
        if (deliveryStatusInteger <= 31) {
            deliveryStatus = DeliveryStatusType.DELIVERED;
        } else if (deliveryStatusInteger >= 32 && deliveryStatusInteger <= 63) {
            deliveryStatus = DeliveryStatusType.REPEATING_DELIVERY;
        } else if (deliveryStatusInteger >= 64 && deliveryStatusInteger <= 127) {
            deliveryStatus = DeliveryStatusType.UNABLE_TO_DELIVER;
        }
    }

    @Override
    public String toString() {
        return "From :" + senderAddress.getNumber() + " referenceNumber: " + referenceNumber + " time: " + smsTimeStamp;
    }

    public String getSmsPduHexString() {
        return smsPduHexString;
    }

    public SmsAddress getSmscAddress() {
        return smscAddress;
    }

    public SmsAddress getSenderAddress() {
        return senderAddress;
    }

    public SmsTimeStamp getSmsTimeStamp() {
        return smsTimeStamp;
    }

    public Integer getReferenceNumber() {
        return referenceNumber;
    }

    public DeliveryStatusType getDeliveryStatus() {
        return deliveryStatus;
    }

    public enum DeliveryStatusType {
        DELIVERED, REPEATING_DELIVERY, UNABLE_TO_DELIVER
    }
}
