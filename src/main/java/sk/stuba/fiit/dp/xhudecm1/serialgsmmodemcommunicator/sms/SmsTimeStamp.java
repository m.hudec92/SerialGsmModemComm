/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class SmsTimeStamp {

    /**
     * Raw hex representation
     */
    String timeStamp;
    int year; //These semi-octets are in "Swapped Nibble" mode
    int month; //BCD code where nibbles within octet is swapped. E.g.: 0x31 Represents value of 13
    int day; //E.g.: 0x99 0x20 0x21 0x50 0x75 0x03 0x21 means 12. Feb 1999 05:57:30 GMT+3
    int hour;
    int minute;
    int second;
    private int timezone;

    float getGMT() {
        return (float) timezone / 4;
    }

    int getMinutesDifferenceGMT() {
        return timezone * 15;
    }

    SmsTimeStamp(String hexString) {
        timeStamp = hexString;
        String swapped = Utils.swapHexString(timeStamp);
        year = Integer.parseInt(swapped.substring(0, 2));
        month = Integer.parseInt(swapped.substring(2, 4));
        day = Integer.parseInt(swapped.substring(4, 6));
        hour = Integer.parseInt(swapped.substring(6, 8));
        minute = Integer.parseInt(swapped.substring(8, 10));
        second = Integer.parseInt(swapped.substring(10, 12));
        timezone = Integer.parseInt(swapped.substring(12, 14));
    }

    public String toString() {
        return "timeStamp=" + timeStamp + ", year=" + year + ", month=" + month + ", day=" + day + ", hour=" + hour + ", minute=" + minute + ", second=" + second + ", timezone=" + getGMT();
    }

}
