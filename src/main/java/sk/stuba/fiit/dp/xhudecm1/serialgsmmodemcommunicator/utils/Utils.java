/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCOutCMGR;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsAddress;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.sms.SmsPDU;
import sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions.UnableToRetrieveNewMessageContentException;

/**
 *
 * @author martinhudec
 */
public class Utils {

    final static Logger logger = Logger.getLogger(Utils.class);

    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    public static byte[] toByteArray(String s) {
        return DatatypeConverter.parseHexBinary(s);
    }

    public static String AddrToPDU(String s) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        s = s.substring(1, s.length());
        if (s.length() % 2 > 0) {
            s += 'F';
        }

        while (i < s.length()) {
            sb.append(s.charAt(i + 1));
            sb.append(s.charAt(i));

            i += 2;
        }
        return sb.toString();
    }
    private static char[] intToHexBuffer;
    private static final char[] hexChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static synchronized String intToHexFixedWidth(int num, int w) {
        if (w < 0) {
            return null;
        }
        if (intToHexBuffer == null || intToHexBuffer.length < w) {
            intToHexBuffer = new char[w];
        }
        for (int i = 0; i < w; i++) {
            intToHexBuffer[w - i - 1] = hexChars[((num >>> (4 * i)) & (0x0F))];
        }
        return new String(intToHexBuffer, 0, w);
    }

    public static String hexStringSwapChars(String hex) {

        if (hex.length() % 2 == 1) {
            return null;
        }
        String result = "";
        for (int i = 0; i < hex.length(); i++) {
            result += (i % 2 == 0 ? hex.charAt(i + 1) : hex.charAt(i - 1));
        }
        return result;
    }

    public static boolean getBitValAtPos(int num, int pos) throws IllegalArgumentException {
       
        if (pos >= 0) {
            return ((num >> pos) & 1) == 1;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static int unsignedIntFromInteger(int n, int from, int to) throws IllegalArgumentException {
       
        int result = 0;
        int i = 0;

        if (from <= to) {
            while (from + i <= to) {
                result += (getBitValAtPos(n, from + i) ? 1 << i : 0);
                i++;
            }
            return result;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public static String parseNumberFromHexStringPDU(String hex, SmsAddress address, int addressLength) {
        String number;
        //logger.info("parsing number " + address.getNumberType());
        if (address.getNumberType() == SmsAddress.NumberType.ALPHANUMERIC) {
            logger.error("ALPHANUMERIC number "  + address.getNumber());
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        } else {
            number = hexStringSwapChars(hex);
            if (number.endsWith("F")) {
                number = number.substring(0, number.length() - 1);
            }
            if (address.getNumberingPlan() == SmsAddress.NumberingPlan.TELEPHONE) {
                number = "+" + number;
            }
        }
        return number;
    }

    public static String getRegexValue(String string, String regex) throws UnableToFindValueWithRegexException {
        Matcher matcher = Pattern.compile(regex).matcher(string);

        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new UnableToFindValueWithRegexException("Nothing found with", string, regex);
        }
    }

    public static class UnableToFindValueWithRegexException extends Exception {

        public UnableToFindValueWithRegexException(String message, String string, String regex) {
            super(message + " regex: " + regex + " in " + string);
        }
    }

    public static String swapHexString(String hex) {
        
        if (hex.length() % 2 == 1) {
            return null;
        }
        String result = "";
        for (int i = 0; i < hex.length(); i++) {
            result += (i % 2 == 0 ? hex.charAt(i + 1) : hex.charAt(i - 1));
        }
        return result;
    }
}
