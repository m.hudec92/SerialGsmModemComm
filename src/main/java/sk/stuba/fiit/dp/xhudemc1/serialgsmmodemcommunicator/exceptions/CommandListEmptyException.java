/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

/**
 *
 * @author martinhudec
 */
public class CommandListEmptyException extends SerialGsmModemCommunicatorException {

    public CommandListEmptyException(String message) {
        super(message);
    }
}
