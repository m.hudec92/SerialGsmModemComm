/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

/**
 *
 * @author martinhudec
 */
public class CommandNotSupportedException extends SerialGsmModemCommunicatorException{

    public CommandNotSupportedException(String message) {
        super(message);
    }
   
}
