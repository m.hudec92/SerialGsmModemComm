/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;

/**
 *
 * @author martinhudec
 */
public class ErrorWritingATCommandToSerialPortException extends SerialPortOperationException {

    public ErrorWritingATCommandToSerialPortException(String message, ATCInAbstract ATcommand, SerialComm serialPortSingleton) {
        super(message + " ATCommand: " + ATcommand.getATCommand(), serialPortSingleton.getSerialComPortName());
    }

    public ErrorWritingATCommandToSerialPortException(String message, ATCInAbstract ATcommand, SerialComm serialPortSingleton, Throwable cause) {
        super(message + " ATCommand: " + ATcommand.getATCommand(), serialPortSingleton.getSerialComPortName(), cause);
    }

}
