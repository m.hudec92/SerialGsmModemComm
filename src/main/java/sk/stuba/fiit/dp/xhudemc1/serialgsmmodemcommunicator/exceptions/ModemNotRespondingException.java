/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;

/**
 *
 * @author martinhudec
 */
public class ModemNotRespondingException extends SerialPortOperationException {

    public ModemNotRespondingException(String message, SerialComm serialPort) {
        super(message, serialPort.getSerialComPortName());
    }

    public ModemNotRespondingException(String message, SerialComm serialPort, Throwable cause) {
        super(message, serialPort.getSerialComPortName(), cause);
    }

}
