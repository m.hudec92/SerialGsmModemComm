/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;

/**
 *
 * @author martinhudec
 */
public class SerialGsmModemCommunicatorException extends Exception{

    public SerialGsmModemCommunicatorException(String message) {
        super(message);
    }

    public SerialGsmModemCommunicatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
