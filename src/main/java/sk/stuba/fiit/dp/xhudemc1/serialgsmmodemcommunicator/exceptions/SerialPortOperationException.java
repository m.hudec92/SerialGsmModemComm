/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

/**
 *
 * @author martinhudec
 */
public class SerialPortOperationException extends SerialGsmModemCommunicatorException {

    public SerialPortOperationException(String message, String serialPortName) {
        super(message + " SerialPort " + serialPortName);
    }
    public SerialPortOperationException(String message, String serialPortName, Throwable cause) {
        super(message + " SerialPort " + serialPortName, cause);
    }
}
