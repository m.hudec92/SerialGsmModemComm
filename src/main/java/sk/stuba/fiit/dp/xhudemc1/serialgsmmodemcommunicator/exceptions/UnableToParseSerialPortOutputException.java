/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;

/**
 *
 * @author martinhudec
 */
public class UnableToParseSerialPortOutputException extends SerialPortOperationException {

    public UnableToParseSerialPortOutputException(String message, String readResult, SerialComm serialPortSingleton) {
        super(message + " ATCommand: " + "\n" + " serial port output: " + readResult.trim(), serialPortSingleton.getSerialComPortName());
    }
}
