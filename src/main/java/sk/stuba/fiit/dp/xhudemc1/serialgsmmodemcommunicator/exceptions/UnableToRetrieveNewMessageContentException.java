/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class UnableToRetrieveNewMessageContentException extends Exception {

    public UnableToRetrieveNewMessageContentException(String message, String atCommandResult) {
        super(message + " ATCommandResult: " + atCommandResult);
    }

    public UnableToRetrieveNewMessageContentException(String message, String atCommandResult, Throwable cause) {
        super(message + " ATCommandResult: " + atCommandResult, cause);
    }

    public UnableToRetrieveNewMessageContentException(String error_retrieving_data_while_reading_SMS_m, Utils.UnableToFindValueWithRegexException ex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
