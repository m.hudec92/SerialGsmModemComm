/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sk.stuba.fiit.dp.xhudemc1.serialgsmmodemcommunicator.exceptions;

import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.modem.ATCommands.ATCInAbstract;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.serialinterface.SerialComm;
import sk.stuba.fiit.dp.xhudecm1.serialgsmmodemcommunicator.utils.Utils;

/**
 *
 * @author martinhudec
 */
public class UnableToRetrieveSentMessageReferenceNumber extends Exception {

    public UnableToRetrieveSentMessageReferenceNumber(String message, String atCommandResult) {
        super(message + " ATCommandResult: " + atCommandResult);
    }

    public UnableToRetrieveSentMessageReferenceNumber(String message, String atCommandResult, Throwable cause) {
        super(message + " ATCommandResult: " + atCommandResult, cause);
    }

}
